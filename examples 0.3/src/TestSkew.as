package {
	import away3d.containers.ObjectContainer3D;
	import away3d.entities.Mesh;
	import away3d.materials.TextureMaterial;
	import away3d.primitives.CubeGeometry;
	import away3d.textures.BitmapTexture;
	
	import com.as3dmod.ModifierStack;
	import com.as3dmod.modifiers.Skew;
	import com.as3dmod.plugins.away3d4.LibraryAway3d4;
	import com.as3dmod.util.ModConstant;
	
	import flash.display.DisplayObjectContainer;
	import flash.utils.Dictionary;
	
	/**
	 * Тест модификатора Skew.
	 * @author redefy
	 */
	public class TestSkew extends BaseTest{
		[Embed(source = "resources/textures/texture09.jpg")] public const Texture0:Class;
		
		private var _mesh:Mesh;
		private var _modifierStack:ModifierStack;
		private var _property:Dictionary;
		
		/**
		 * Конструктор.
		 * @param	parent		ссылка на DisplayObjectContainer в который будет добавлен GUI.
		 * @param	conteiner	ссылка на ObjectContainer3D в который будут добавлены объекты.
		 */
		public function TestSkew(parent:DisplayObjectContainer, conteiner:ObjectContainer3D) {
			super(parent, conteiner);
		}
		
		/** @inheritDoc */
		override protected function initObjects():void {
			var texture:BitmapTexture = new BitmapTexture(new Texture0().bitmapData);
			var material:TextureMaterial = new TextureMaterial(texture, true, true, true);
			var geometry:CubeGeometry = new CubeGeometry(300, 400, 300, 10, 10, 10, false);
			_mesh = new Mesh(geometry, material);
		}
		
		/** @inheritDoc */
		override protected function initModificator():void {
			_modifierStack = new ModifierStack(new LibraryAway3d4(), _mesh);
			_modifier = new Skew();
			_modifierStack.addModifier(_modifier);
			
			_property = new Dictionary();
			_property["force"] = Skew(_modifier).force;
			_property["constraint"] = Skew(_modifier).constraint;
			_property["offset"] = Skew(_modifier).offset;
			_property["power"] = Skew(_modifier).power;
			_property["falloff"] = Skew(_modifier).falloff;
			_property["oneSide"] = Skew(_modifier).oneSide;
			_property["skewAxis"] = Skew(_modifier).skewAxis;
			_property["swapAxes"] = Skew(_modifier).swapAxes;
			_property["inverseFalloff"] = Skew(_modifier).inverseFalloff;
		}
		
		/** @inheritDoc */
		override protected function initGUI():void {
			_gui = new SimpleGUI(_parent, "TEST AS3DMOD 0.3", "");
			_gui.addGroup("CHOOSE MODIFIER:");
			_gui.addComboBox("test", [
										{label:"Bend",					data:0},
										{label:"BitmapDisplacement",	data:1},
										{label:"Bloat",					data:2},
										{label:"Break",					data:3},
										{label:"Cloth",					data:4},
										{label:"Noise",					data:5},
										{label:"Perlin",				data:6},
										{label:"Skew",					data:7},
										{label:"Taper",					data:8},
										{label:"Twist",					data:9},
										{label:"Wheel",					data:10}
			]);
			
			_gui.addButton("Reset Camera",   { callback:Base(_parent).resetCamera, width:150 } );
			_gui.addButton("Reset Modifier", { callback:resetModifier, width:150 } );
			
			_gui.addColumn("Skew Properties");
			_gui.addSlider("modifier.force", -300, 300, { label:"Force",tick:0.1 });
			_gui.addSlider("modifier.power", 1, 3, 	 	{ label:"Power",   tick:0.1 });
			_gui.addSlider("modifier.offset", 0, 1,  	{ label:"Offset",  tick:0.1 });
			_gui.addSlider("modifier.falloff", 0, 1, 	{ label:"Falloff", tick:0.1 });
			
			_gui.addComboBox("modifier.constraint", [
										{label:"ModConstant.NONE",		data:ModConstant.NONE},
										{label:"ModConstant.LEFT",		data:ModConstant.LEFT}, 
										{label:"ModConstant.RIGHT",		data:ModConstant.RIGHT}
			], { label:"Constraint" } );
			
			_gui.addComboBox("modifier.skewAxis", [
										{label:"ModConstant.X",		data:ModConstant.X},
										{label:"ModConstant.Y",		data:ModConstant.Y}, 
										{label:"ModConstant.Z",		data:ModConstant.Z}
			], { label:"SkewAxis" } );
			
			_gui.addToggle("modifier.oneSide", { label:"OneSide" } );
			_gui.addToggle("modifier.swapAxes", { label:"SwapAxes" } );
			_gui.addToggle("modifier.inverseFalloff", { label:"InverseFalloff" } );
		}
		
		/** @inheritDoc */
		override public function enable():void {
			_conteiner.addChild(_mesh);
			
			if (!_gui) initGUI();
			_gui.show();
		}
		
		/** @inheritDoc */
		override public function disable():void {
			_conteiner.removeChild(_mesh);
			
			_gui.hide();
		}
		
		/** @inheritDoc */
		override public function resetModifier():void {
			_property["force"] = Skew(_modifier).force = 0;
			_property["constraint"] = Skew(_modifier).constraint = ModConstant.NONE; 
			_property["offset"] = Skew(_modifier).offset = 0.5;
			_property["power"] = Skew(_modifier).power = 1;
			_property["falloff"] = Skew(_modifier).falloff = 1;
			_property["oneSide"] = Skew(_modifier).oneSide = false;
			_property["skewAxis"] = Skew(_modifier).skewAxis =  ModConstant.Y;
			_property["swapAxes"] = Skew(_modifier).swapAxes = false;
			_property["inverseFalloff"] = Skew(_modifier).inverseFalloff = false;
			_modifierStack.apply();
		}
		
		/** @inheritDoc */
		override public function update():void { 
			for (var p:String in _property) { 
				if (_property[p] != Skew(_modifier)[p]) {
					_property[p] = Skew(_modifier)[p];
					_modifierStack.apply();
					return;
				}
			}
		}
	}
}