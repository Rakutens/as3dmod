package {
	import away3d.containers.ObjectContainer3D;
	import away3d.entities.Mesh;
	import away3d.materials.TextureMaterial;
	import away3d.primitives.CubeGeometry;
	import away3d.textures.BitmapTexture;
	
	import com.as3dmod.ModifierStack;
	import com.as3dmod.modifiers.Bloat;
	import com.as3dmod.plugins.away3d4.LibraryAway3d4;
	
	import flash.display.DisplayObjectContainer;
	import flash.geom.Vector3D;
	import flash.utils.Dictionary;
	
	/**
	 * Тест модификатора Bloat.
	 * @author redefy
	 */
	public class TestBloat extends BaseTest{
		[Embed(source = "resources/textures/texture00.jpg")] public const Texture0:Class;
		
		private var _mesh:Mesh;
		private var _modifierStack:ModifierStack;
		private var _property:Dictionary;
		private var _centerProperty:Array = [new Vector3D(0, 0, 0), new Vector3D(0, 90, 0), new Vector3D(0, -90, 0), new Vector3D(-90, 0, 0), new Vector3D(90, 0, 0)];
		
		/**
		 * Конструктор.
		 * @param	parent		ссылка на DisplayObjectContainer в который будет добавлен GUI.
		 * @param	conteiner	ссылка на ObjectContainer3D в который будут добавлены объекты.
		 */
		public function TestBloat(parent:DisplayObjectContainer, conteiner:ObjectContainer3D) {
			super(parent, conteiner);
		}
		
		/** @inheritDoc */
		override protected function initObjects():void {
			var texture:BitmapTexture = new BitmapTexture(new Texture0().bitmapData);
			var material:TextureMaterial = new TextureMaterial(texture, true, true, true);
			var geometry:CubeGeometry = new CubeGeometry(350, 350, 350, 10, 10, 10, false);
				
			_mesh = new Mesh(geometry, material);
		}
		
		/** @inheritDoc */
		override protected function initModificator():void {
			_modifierStack = new ModifierStack(new LibraryAway3d4(), _mesh);
			_modifier = new Bloat();
			_modifierStack.addModifier(_modifier);
			
			_property = new Dictionary();
			_property["a"] = Bloat(_modifier).a;
			_property["radius"] = Bloat(_modifier).radius;
			_property["center"] = Bloat(_modifier).center = _centerProperty[0];
		}
		
		/** @inheritDoc */
		override protected function initGUI():void {
			_gui = new SimpleGUI(_parent, "TEST AS3DMOD 0.3", "");
			_gui.addGroup("CHOOSE MODIFIER:");
			_gui.addComboBox("test", [
										{label:"Bend",					data:0},
										{label:"BitmapDisplacement",	data:1},
										{label:"Bloat",					data:2},
										{label:"Break",					data:3},
										{label:"Cloth",					data:4},
										{label:"Noise",					data:5},
										{label:"Perlin",				data:6},
										{label:"Skew",					data:7},
										{label:"Taper",					data:8},
										{label:"Twist",					data:9},
										{label:"Wheel",					data:10}
			]);
			
			_gui.addButton("Reset Camera",   { callback:Base(_parent).resetCamera, width:150 } );
			_gui.addButton("Reset Modifier", { callback:resetModifier, width:150 } );
			
			_gui.addColumn("Bloat Properties");
			_gui.addComboBox("modifier.a", [
											{label:"0.01",		data:0.01},
											{label:"0.02",		data:0.02},
											{label:"0.03",		data:0.03},
			], { label:"A"});
			
			_gui.addSlider("modifier.radius", 0, 500, { label:"Radius", tick:0.1 } );
			
			_gui.addComboBox("modifier.center", [
													{label:"Center",	data:_centerProperty[0]},
													{label:"Up",		data:_centerProperty[1]}, 
													{label:"Down",		data:_centerProperty[2]},
													{label:"Left",		data:_centerProperty[3]},
													{label:"Right",		data:_centerProperty[4]},
			], { label:"Center" } );
		}
		
		/** @inheritDoc */
		override public function enable():void {
			_conteiner.addChild(_mesh);
			
			if (!_gui) initGUI();
			_gui.show();
		}
		
		/** @inheritDoc */
		override public function disable():void {
			_conteiner.removeChild(_mesh);
			
			_gui.hide();
		}
		
		/** @inheritDoc */
		override public function resetModifier():void {
			_property["a"] = Bloat(_modifier).a	= 0.01;
			_property["radius"] = Bloat(_modifier).radius = 0;
			_property["center"] = Bloat(_modifier).center = _centerProperty[0];
			_modifierStack.apply();
		}
		
		/** @inheritDoc */
		override public function update():void { 
			for (var p:String in _property) { 
				if (_property[p] != Bloat(_modifier)[p]) {
					_property[p] = Bloat(_modifier)[p];
					_modifierStack.apply();
					return;
				}
			}
		}
	}
}