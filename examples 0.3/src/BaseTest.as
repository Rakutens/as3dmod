package {
	import away3d.containers.ObjectContainer3D;
	import away3d.entities.Mesh;
	
	import com.as3dmod.IModifier;
	
	import flash.display.DisplayObjectContainer;
	
	/**
	 * Базовый класс для всех классов-тестов модификаторов.
	 * @author redefy
	 */
	public class BaseTest {
		protected var _parent:DisplayObjectContainer;
		protected var _conteiner:ObjectContainer3D;
		protected var _modifier:IModifier;
		protected var _gui:SimpleGUI;
		
		/**
		 * Конструктор.
		 * @param	parent		ссылка на DisplayObjectContainer в который будет добавлен GUI.
		 * @param	conteiner	ссылка на ObjectContainer3D в который будут добавлены объекты.
		 */
		public function BaseTest(parent:DisplayObjectContainer, conteiner:ObjectContainer3D) {
			_parent = parent;
			_conteiner = conteiner;
			
			initObjects();
			initModificator();
		}
		
		/** Инициализация объектов. */
		protected function initObjects():void { }
		
		/** Инициализация модификатора. */
		protected function initModificator():void { }
		
		/** Инициализация GUI. */
		protected function initGUI():void { }
		
		/** Ссылка на модификатор. */
		public function get modifier():IModifier { return _modifier; }
		
		/** Активирование модификатора. */
		public function enable():void { }
		
		/** Деактивирование модификатора. */
		public function disable():void { }
		
		/** Возвращение геометрии меша в состояние, которое было у нее до применения модификатора.  */
		public function resetModifier():void {}
		
		/** Применение модификатора к объектам. */
		public function update():void { }
	}
}