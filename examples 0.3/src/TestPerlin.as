package {
	import away3d.containers.ObjectContainer3D;
	import away3d.entities.Mesh;
	import away3d.materials.TextureMaterial;
	import away3d.primitives.PlaneGeometry;
	import away3d.textures.BitmapTexture;
	
	import com.as3dmod.ModifierStack;
	import com.as3dmod.modifiers.Perlin;
	import com.as3dmod.plugins.away3d4.LibraryAway3d4;
	import com.as3dmod.util.ModConstant;
	import com.as3dmod.util.bitmap.PerlinNoise;
	
	import flash.display.BitmapDataChannel;
	import flash.display.DisplayObjectContainer;
	import flash.utils.Dictionary;
	
	/**
	 * Тест модификатора Perlin.
	 * @author redefy
	 */
	public class TestPerlin extends BaseTest{
		[Embed(source = "resources/textures/texture04.jpg")] public const Texture0:Class;
		[Embed(source = "resources/textures/texture05.jpg")] public const Texture1:Class;
		[Embed(source = "resources/textures/texture06.jpg")] public const Texture2:Class;
		
		private var _mesh:Vector.<Mesh>;
		private var _modifierStack:ModifierStack;
		private var _property:Dictionary;
		private var _perlinNoise:PerlinNoise;
		
		/**
		 * Конструктор.
		 * @param	parent		ссылка на DisplayObjectContainer в который будет добавлен GUI.
		 * @param	conteiner	ссылка на ObjectContainer3D в который будут добавлены объекты.
		 */
		public function TestPerlin(parent:DisplayObjectContainer, conteiner:ObjectContainer3D) {
			super(parent, conteiner);
		}
		
		/** @inheritDoc */
		override protected function initObjects():void {
			var texture0:BitmapTexture = new BitmapTexture(new Texture0().bitmapData);
			var texture1:BitmapTexture = new BitmapTexture(new Texture1().bitmapData);
			var texture2:BitmapTexture = new BitmapTexture(new Texture2().bitmapData);
			var material0:TextureMaterial = new TextureMaterial(texture0, true, true, true);
			var material1:TextureMaterial = new TextureMaterial(texture1, true, true, true);
			var material2:TextureMaterial = new TextureMaterial(texture2, true, true, true);
			var geometry:PlaneGeometry = new PlaneGeometry(500, 250, 10, 10, true);
			var indices:Vector.<uint> = geometry.subGeometries[0].indexData.slice();
			var sil:uint = indices.length;
		
			for (var i:int = 0; i < sil; i += 3)
				indices.push(indices[i + 2], indices[i + 1], indices[i]);
			
			geometry.subGeometries[0].updateIndexData(indices);
				
			_mesh = new Vector.<Mesh>(3, true);
			_mesh[0] = new Mesh(geometry, material0);
			_mesh[0].z = 300;
			_mesh[1] = new Mesh(geometry, material1);
			_mesh[2] = new Mesh(geometry, material2);
			_mesh[2].z = -300;
		}
		
		/** @inheritDoc */
		override protected function initModificator():void {
			_modifierStack = new ModifierStack(new LibraryAway3d4(), _mesh[0]);
			
			_perlinNoise = new PerlinNoise(25, 25);
			_perlinNoise.baseX = 50;
			_perlinNoise.baseY = 50;
			_perlinNoise.channels = BitmapDataChannel.RED | BitmapDataChannel.GREEN | BitmapDataChannel.BLUE; 
			_perlinNoise.octaves = 2;
			
			_modifier = new Perlin(1, _perlinNoise, true);
			_modifierStack.addModifier(_modifier);
			
			_property = new Dictionary();
			_property["axes"] = Perlin(_modifier).axes = ModConstant.Y;
			_property["autoRun"] = Perlin(_modifier).autoRun;
			_property["force"] = Perlin(_modifier).force;
			_property["speedX"] = Perlin(_modifier).speedX;
			_property["speedY"] = Perlin(_modifier).speedY;
		}
		
		/** @inheritDoc */
		override protected function initGUI():void {
			_gui = new SimpleGUI(_parent, "TEST AS3DMOD 0.3", "");
			_gui.addGroup("CHOOSE MODIFIER:");
			_gui.addComboBox("test", [
										{label:"Bend",					data:0},
										{label:"BitmapDisplacement",	data:1},
										{label:"Bloat",					data:2},
										{label:"Break",					data:3},
										{label:"Cloth",					data:4},
										{label:"Noise",					data:5},
										{label:"Perlin",				data:6},
										{label:"Skew",					data:7},
										{label:"Taper",					data:8},
										{label:"Twist",					data:9},
										{label:"Wheel",					data:10}
			]);
			
			_gui.addButton("Reset Camera",   { callback:Base(_parent).resetCamera, width:150 } );
			_gui.addButton("Reset Modifier", { callback:resetModifier, width:150 } );
			
			_gui.addColumn("Perlin Properties");
			_gui.addSlider("modifier.force", -5, 5, { label:"Force", tick:0.1 });
			_gui.addSlider("modifier.speedX", 0, 1, { label:"SpeedX", tick:0.1 });
			_gui.addSlider("modifier.speedY", 0, 1, { label:"SpeedY", tick:0.1 } );
			
			_gui.addComboBox("modifier.axes", [
										{label:"X",		data:ModConstant.X},
										{label:"Y",		data:ModConstant.Y}, 
										{label:"XYZ",	data:ModConstant.X | ModConstant.Y | ModConstant.Z}
			], { label:"Axes" } );
			
			_gui.addToggle("modifier.autoRun", { label:"AutoRun" } );
		}
		
		/** @inheritDoc */
		override public function enable():void {
			for (var i:int = 0; i < 3; i++) _conteiner.addChild(_mesh[i]);
			
			if (!_gui) initGUI();
			_gui.show();
		}
		
		/** @inheritDoc */
		override public function disable():void {
			for (var i:int = 0; i < 3; i++) _conteiner.removeChild(_mesh[i]);
			
			_gui.hide();
		}
		
		/** @inheritDoc */
		override public function resetModifier():void {
			_property["axes"] = Perlin(_modifier).axes = ModConstant.Y;
			_property["autoRun"] = Perlin(_modifier).autoRun = true;
			_property["force"] = Perlin(_modifier).force = 1;
			_property["speedX"] = Perlin(_modifier).speedX = 1;
			_property["speedY"] = Perlin(_modifier).speedY = 0;
			_modifierStack.apply();
		}
		
		/** @inheritDoc */
		override public function update():void { 
			_modifierStack.apply();
		}
	}
}