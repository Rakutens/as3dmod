package {
	import away3d.containers.ObjectContainer3D;
	import away3d.entities.Mesh;
	import away3d.materials.TextureMaterial;
	import away3d.primitives.PlaneGeometry;
	import away3d.textures.BitmapTexture;
	
	import com.as3dmod.ModifierStack;
	import com.as3dmod.modifiers.Bend;
	import com.as3dmod.plugins.away3d4.LibraryAway3d4;
	import com.as3dmod.util.ModConstant;
	
	import flash.display.DisplayObjectContainer;
	import flash.utils.Dictionary;
	
	/**
	 * Тест модификатора Bend.
	 * @author redefy
	 */
	public class TestBend extends BaseTest{
		[Embed(source = "resources/textures/texture12.jpg")] public const Texture0:Class;
		
		private var _mesh:Mesh;
		private var _modifierStack:ModifierStack;
		private var _property:Dictionary;
		
		/**
		 * Конструктор.
		 * @param	parent		ссылка на DisplayObjectContainer в который будет добавлен GUI.
		 * @param	conteiner	ссылка на ObjectContainer3D в который будут добавлены объекты.
		 */
		public function TestBend(parent:DisplayObjectContainer, conteiner:ObjectContainer3D) {
			super(parent, conteiner);
		}
		
		/** @inheritDoc */
		override protected function initObjects():void {
			var texture:BitmapTexture = new BitmapTexture(new Texture0().bitmapData);
			var material:TextureMaterial = new TextureMaterial(texture, true, true, true);
			var geometry:PlaneGeometry = new PlaneGeometry(800, 500, 15, 15, true);
			var indices:Vector.<uint> = geometry.subGeometries[0].indexData.slice();
			var sil:uint = indices.length;
		
			for (var i:int = 0; i < sil; i += 3)
				indices.push(indices[i + 2], indices[i + 1], indices[i]);
			
			geometry.subGeometries[0].updateIndexData(indices);
			
			_mesh = new Mesh(geometry, material);
		}
		
		/** @inheritDoc */
		override protected function initModificator():void {
			_modifierStack = new ModifierStack(new LibraryAway3d4(), _mesh);
			_modifier = new Bend();
			_modifierStack.addModifier(_modifier);
			
			_property = new Dictionary();
			_property["bendAxis"] = Bend(_modifier).bendAxis = ModConstant.X;
			_property["constraint"] = Bend(_modifier).constraint;
			_property["force"] = Bend(_modifier).force;
			_property["offset"] = Bend(_modifier).offset;
			_property["pointAxis"] = Bend(_modifier).pointAxis = ModConstant.Y;
		}
		
		/** @inheritDoc */
		override protected function initGUI():void {
			_gui = new SimpleGUI(_parent, "TEST AS3DMOD 0.2", "");
			_gui.addGroup("CHOOSE MODIFIER:");
			_gui.addComboBox("test", [
										{label:"Bend",		data:0},
										{label:"Bloat",		data:1},
										{label:"Noise",		data:2},
										{label:"Perlin",	data:3},
										{label:"Skew",		data:4},
										{label:"Taper",		data:5},
										{label:"Twist",		data:6}
			]);
			
			_gui.addButton("Reset Camera",   { callback:Base(_parent).resetCamera, width:150 } );
			_gui.addButton("Reset Modifier", { callback:resetModifier, width:150 } );
			
			_gui.addColumn("Bend Properties");
			_gui.addComboBox("modifier.bendAxis", [
										{label:"ModConstant.X",		data:ModConstant.X},
										{label:"ModConstant.Y",		data:ModConstant.Y}, 
										{label:"ModConstant.Z",		data:ModConstant.Z }
			], { label:"BendAxis" } );
			
			_gui.addComboBox("modifier.constraint", [
										{label:"ModConstant.NONE",		data:ModConstant.NONE},
										{label:"ModConstant.LEFT",		data:ModConstant.LEFT}, 
										{label:"ModConstant.RIGHT",		data:ModConstant.RIGHT}
			], { label:"Constraint" } );
										
			_gui.addSlider("modifier.force", -5, 5, { label:"Force", tick:0.1 } );
			_gui.addSlider("modifier.offset", 0, 1, { label:"Offset", tick:0.1 } );
			
			_gui.addComboBox("modifier.pointAxis", [
										{label:"ModConstant.X",		data:ModConstant.X},
										{label:"ModConstant.Y",		data:ModConstant.Y}, 
										{label:"ModConstant.Z",		data:ModConstant.Z }
			], { label:"PointAxis" } );
		}
		
		/** @inheritDoc */
		override public function enable():void {
			_conteiner.addChild(_mesh);
			
			if (!_gui) initGUI();
			_gui.show();
		}
		
		/** @inheritDoc */
		override public function disable():void {
			_conteiner.removeChild(_mesh);
			
			_gui.hide();
		}
		
		/** @inheritDoc */
		override public function resetModifier():void {
			_property["bendAxis"] = Bend(_modifier).bendAxis = ModConstant.X;
			_property["constraint"] = Bend(_modifier).constraint = ModConstant.NONE;
			_property["force"] = Bend(_modifier).force = 0.0;
			_property["offset"] = Bend(_modifier).offset = 0.5;
			_property["pointAxis"] = Bend(_modifier).pointAxis = ModConstant.Y;
			_modifierStack.apply();
		}
		
		/** @inheritDoc */
		override public function update():void { 
			for (var p:String in _property) { 
				if (_property[p] != Bend(_modifier)[p]) {
					_property[p] = Bend(_modifier)[p];
					_modifierStack.apply();
					return;
				}
			}
		}
	}
}