package {
	import away3d.containers.ObjectContainer3D;
	import away3d.entities.Mesh;
	import away3d.materials.TextureMaterial;
	import away3d.primitives.CubeGeometry;
	import away3d.textures.BitmapTexture;
	
	import com.as3dmod.ModifierStack;
	import com.as3dmod.modifiers.Noise;
	import com.as3dmod.plugins.away3d4.LibraryAway3d4;
	import com.as3dmod.util.ModConstant;
	
	import flash.display.DisplayObjectContainer;
	import flash.utils.Dictionary;
	
	/**
	 * Тест модификатора Noise.
	 * @author redefy
	 */
	public class TestNoise extends BaseTest{
		[Embed(source = "resources/textures/texture03.jpg")] public const Texture0:Class;
		
		private var _mesh:Mesh;
		private var _modifierStack:ModifierStack;
		private var _property:Dictionary;
		
		/**
		 * Конструктор.
		 * @param	parent		ссылка на DisplayObjectContainer в который будет добавлен GUI.
		 * @param	conteiner	ссылка на ObjectContainer3D в который будут добавлены объекты.
		 */
		public function TestNoise(parent:DisplayObjectContainer, conteiner:ObjectContainer3D) {
			super(parent, conteiner);
		}
		
		/** @inheritDoc */
		override protected function initObjects():void {
			var texture:BitmapTexture = new BitmapTexture(new Texture0().bitmapData);
			var material:TextureMaterial = new TextureMaterial(texture, true, true, true);
			var geometry:CubeGeometry = new CubeGeometry(350, 350, 350, 20, 20,20, false);
				
			_mesh = new Mesh(geometry, material);
		}
		
		/** @inheritDoc */
		override protected function initModificator():void {
			_modifierStack = new ModifierStack(new LibraryAway3d4(), _mesh);
			_modifier = new Noise();
			_modifierStack.addModifier(_modifier);
			
			_property = new Dictionary();
			_property["force"] = Noise(_modifier).force;
			_property["constraintAxes"] = Noise(_modifier).constraintAxes = ModConstant.X;
			_property["falloffStart"] = Noise(_modifier).falloffStart;
			_property["falloffEnd"] = Noise(_modifier).falloffEnd;
		}
		
		/** @inheritDoc */
		override protected function initGUI():void {
			_gui = new SimpleGUI(_parent, "TEST AS3DMOD 0.2", "");
			_gui.addGroup("CHOOSE MODIFIER:");
			_gui.addComboBox("test", [
										{label:"Bend",		data:0},
										{label:"Bloat",		data:1},
										{label:"Noise",		data:2},
										{label:"Perlin",	data:3},
										{label:"Skew",		data:4},
										{label:"Taper",		data:5},
										{label:"Twist",		data:6}
			]);
			
			_gui.addButton("Reset Camera",   { callback:Base(_parent).resetCamera, width:150 } );
			_gui.addButton("Reset Modifier", { callback:resetModifier, width:150 } );
			
			_gui.addColumn("Noise Properties");
			_gui.addSlider("modifier.force", -50, 50, { label:"Force", tick:0.1 });
			_gui.addComboBox("modifier.constraintAxes", [
										{label:"ModConstant.X",		data:ModConstant.X},
										{label:"ModConstant.Y",		data:ModConstant.Y}, 
										{label:"ModConstant.Z",		data:ModConstant.Z}
			], { label:"ConstraintAxes" } );
			
			_gui.addSlider("modifier.falloffStart", 0, 1, { label:"FalloffStart", tick:0.1 });
			_gui.addSlider("modifier.falloffEnd", 0, 1, { label:"FalloffEnd", tick:0.1 });
		}
		
		/** @inheritDoc */
		override public function enable():void {
			_conteiner.addChild(_mesh);
			
			if (!_gui) initGUI();
			_gui.show();
		}
		
		/** @inheritDoc */
		override public function disable():void {
			_conteiner.removeChild(_mesh);
			
			_gui.hide();
		}
		
		/** @inheritDoc */
		override public function resetModifier():void {
			_property["force"] = Noise(_modifier).force = 0;
			_property["constraintAxes"] = Noise(_modifier).constraintAxes = ModConstant.NONE;
			_property["falloffStart"] = Noise(_modifier).falloffStart = 0;
			_property["falloffEnd"] = Noise(_modifier).falloffEnd = 0;
			_modifierStack.apply();
		}
		
		/** @inheritDoc */
		override public function update():void { 
			for (var p:String in _property) { 
				if (_property[p] != Noise(_modifier)[p]) {
					_property[p] = Noise(_modifier)[p];
					_modifierStack.apply();
					return;
				}
			}
		}
	}
}