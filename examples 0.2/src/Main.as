package  {
	import com.as3dmod.core.Modifier;
	import com.as3dmod.IModifier;
	import flash.events.Event;
	
	/**
	 * Тестируем модификаторы библиотеки AS3DMod 0.2.
	 * @author redefy
	 */
	public class Main extends Base {
		public var test:int = 0;
		private var currentTest:int;
		
		public var modifier:IModifier;
		
		private var modificators:Vector.<BaseTest>;
		private var currentModificator:BaseTest;
		
		/** Конструктор. */
		public function Main() { super(); }
		
		/** @inheritDoc */
		override protected function initObjects():void {
			modificators = new Vector.<BaseTest>(7, true);
			modificators[0] = new TestBend(this, conteiner);
			modificators[1] = new TestBloat(this, conteiner);
			modificators[2] = new TestNoise(this, conteiner);
			modificators[3] = new TestPerlin(this, conteiner);
			modificators[4] = new TestSkew(this, conteiner);		
			modificators[5] = new TestTaper(this, conteiner);
			modificators[6] = new TestTwist(this, conteiner);
			
			currentModificator = modificators[test];
			modifier = currentModificator.modifier;
			currentModificator.enable();
			
			currentTest = test;
		} 
		
		/** @inheritDoc */
		override protected function update(e:Event):void {
			currentModificator.update();
			super.update(e);
			
			if (test != currentTest) {
				currentModificator.disable();
				currentModificator = modificators[test];
				modifier = currentModificator.modifier;
				currentModificator.enable();
		
				currentTest = test;
			}
		}
	}
}