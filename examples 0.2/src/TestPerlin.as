package {
	import away3d.containers.ObjectContainer3D;
	import away3d.entities.Mesh;
	import away3d.materials.TextureMaterial;
	import away3d.primitives.PlaneGeometry;
	import away3d.textures.BitmapTexture;
	
	import com.as3dmod.ModifierStack;
	import com.as3dmod.modifiers.Perlin;
	import com.as3dmod.plugins.away3d4.LibraryAway3d4;
	
	import flash.display.DisplayObjectContainer;
	import flash.utils.Dictionary;
	
	/**
	 * Тест модификатора Perlin.
	 * @author redefy
	 */
	public class TestPerlin extends BaseTest{
		[Embed(source = "resources/textures/texture04.jpg")] public const Texture0:Class;
		[Embed(source = "resources/textures/texture05.jpg")] public const Texture1:Class;
		[Embed(source = "resources/textures/texture06.jpg")] public const Texture2:Class;
		
		private var _mesh:Vector.<Mesh>;
		private var _modifierStack:ModifierStack;
		private var _property:Dictionary;
		
		/**
		 * Конструктор.
		 * @param	parent		ссылка на DisplayObjectContainer в который будет добавлен GUI.
		 * @param	conteiner	ссылка на ObjectContainer3D в который будут добавлены объекты.
		 */
		public function TestPerlin(parent:DisplayObjectContainer, conteiner:ObjectContainer3D) {
			super(parent, conteiner);
		}
		
		/** @inheritDoc */
		override protected function initObjects():void {
			var texture0:BitmapTexture = new BitmapTexture(new Texture0().bitmapData);
			var texture1:BitmapTexture = new BitmapTexture(new Texture1().bitmapData);
			var texture2:BitmapTexture = new BitmapTexture(new Texture2().bitmapData);
			var material0:TextureMaterial = new TextureMaterial(texture0, true, true, true);
			var material1:TextureMaterial = new TextureMaterial(texture1, true, true, true);
			var material2:TextureMaterial = new TextureMaterial(texture2, true, true, true);
			var geometry:PlaneGeometry = new PlaneGeometry(500, 250, 10, 10, true);
			var indices:Vector.<uint> = geometry.subGeometries[0].indexData.slice();
			var sil:uint = indices.length;
		
			for (var i:int = 0; i < sil; i += 3)
				indices.push(indices[i + 2], indices[i + 1], indices[i]);
			
			geometry.subGeometries[0].updateIndexData(indices);
				
			_mesh = new Vector.<Mesh>(3, true);
			_mesh[0] = new Mesh(geometry, material0);
			_mesh[0].z = 300;
			_mesh[1] = new Mesh(geometry, material1);
			_mesh[2] = new Mesh(geometry, material2);
			_mesh[2].z = -300;
		}
		
		/** @inheritDoc */
		override protected function initModificator():void {
			_modifierStack = new ModifierStack(new LibraryAway3d4(), _mesh[0]);
			_modifier = new Perlin();
			_modifierStack.addModifier(_modifier);
			
			_property = new Dictionary();
			_property["force"] = Perlin(_modifier).force;
			_property["falloffStart"] = Perlin(_modifier).falloffStart;
			_property["falloffEnd"] = Perlin(_modifier).falloffEnd;
		}
		
		/** @inheritDoc */
		override protected function initGUI():void {
			_gui = new SimpleGUI(_parent, "TEST AS3DMOD 0.2", "");
			_gui.addGroup("CHOOSE MODIFIER:");
			_gui.addComboBox("test", [
										{label:"Bend",		data:0},
										{label:"Bloat",		data:1},
										{label:"Noise",		data:2},
										{label:"Perlin",	data:3},
										{label:"Skew",		data:4},
										{label:"Taper",		data:5},
										{label:"Twist",		data:6}
			]);
			
			_gui.addButton("Reset Camera",   { callback:Base(_parent).resetCamera, width:150 } );
			_gui.addButton("Reset Modifier", { callback:resetModifier, width:150 } );
			
			_gui.addColumn("Perlin Properties");
			_gui.addSlider("modifier.force", -5, 5, { label:"Force", tick:0.1 });
			_gui.addSlider("modifier.falloffStart", 0, 1, { label:"FalloffStart", tick:0.1 });
			_gui.addSlider("modifier.falloffEnd", 0, 1, { label:"FalloffEnd", tick:0.1 });
		}
		
		/** @inheritDoc */
		override public function enable():void {
			for (var i:int = 0; i < 3; i++) _conteiner.addChild(_mesh[i]);
			
			if (!_gui) initGUI();
			_gui.show();
		}
		
		/** @inheritDoc */
		override public function disable():void {
			for (var i:int = 0; i < 3; i++) _conteiner.removeChild(_mesh[i]);
			
			_gui.hide();
		}
		
		/** @inheritDoc */
		override public function resetModifier():void {
			_property["force"] = Perlin(_modifier).force = 1;
			_property["falloffStart"] = Perlin(_modifier).falloffStart = 0;
			_property["falloffEnd"] = Perlin(_modifier).falloffEnd = 0;
			_modifierStack.apply();
		}
		
		/** @inheritDoc */
		override public function update():void { 
			_modifierStack.apply();
		}
	}
}